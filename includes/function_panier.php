<?php

    function creationPanier(){
        try{
            $dbs = new PDO('mysql:host=localhost; dbname=boutiqueenligne', 'root','root');
            $dbs->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);//les noms des champs sont en miniscules
            $dbs->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// les erreurs lanceront des exceptions


        }
        catch (Exception $e){
            die('une erreur est survenue');
        }

        //var_dump($_SESSION['panier']);

        if(!isset($_SESSION['panier'])){


            $select = $dbs->query("SELECT TVA FROM Produit ");
            $data = $select->fetch(PDO::FETCH_OBJ);
            $_SESSION['tva'] = $data->tva;
            $_SESSION['verrou'] = false;

        }
        return true;
    }

    function ajouterArticle($id_prd, $libelleProduit, $qteProduit, $prixProduit){

        if(creationPanier() && !isVerrouille()){

            if( isset ($_SESSION['panier'][$id_prd]['qteProduit']) ){

                 $_SESSION['panier'][$id_prd]['qteProduit'] += $qteProduit;

            }else{
                 $_SESSION['panier'][$id_prd]['libelleProduit']   = $libelleProduit;
                 $_SESSION['panier'][$id_prd]['qteProduit']       = $qteProduit;
                 $_SESSION['panier'][$id_prd]['prixProduit']      = $prixProduit;
                 $_SESSION['panier'][$id_prd]['idProduit']        = $id_prd;

            }
        }else{

            echo'Un problème est survenu, veuillez contacter le service technique';
        }

    }


    function modifierQteArticle($idProduitModif, $qteProduit)
    {
        //si le panier existe
        if (creationPanier() && !isVerrouille()) {
            //si la qte est positive  on modif sinon on supp l'article

            if ($qteProduit > 0) {
                if(isset ($_SESSION['panier'][$idProduitModif])) {
                    $_SESSION['panier'][$idProduitModif]['qteProduit'] = $qteProduit;
                }

            } else {
                supprimerArticle($idProduitModif);
            }

        }else {
            echo 'Un problème est survenu, veuillez contacter le service technique';
        }
    }

/**
 * @param $id_prd
 */
    function supprimerArticle($id_prd){

        if(creationPanier() && !isVerrouille()){

            unset($_SESSION['panier'][$id_prd]);

        }else{
            echo'Un problème est survenu, veuillez contacter le service technique';
        }

    }




    function montantGlobal(){

        $total = 0;

        foreach ( $_SESSION['panier'] as $id=> $prd) {
            $total += $_SESSION['panier'][$id]['qteProduit'] * $_SESSION['panier'][$id]['prixProduit'];
        }

        return $total;
    }

    function montantGlobalTVA(){

        $totalTva = 0;

        foreach ( $_SESSION['panier'] as $id=> $prd) {
            $totalTva += $_SESSION['panier'][$id]['qteProduit'] * $_SESSION['panier'][$id]['prixProduit'];
        }

        return $totalTva + ($totalTva * (($_SESSION['tva'])/100));
    }



    function supprimerPanier(){
        unset($_SESSION['panier']);

    }

    function isVerrouille(){
            if(isset($_SESSION['panier']) && $_SESSION['verrou']){

                return true;

            }else{

                return false;
            }

    }


    function compterArticle(){

        if(isset($_SESSION['panier'])){

            return count($_SESSION['panier']['libelleProduit']);
        }
        else{
            return 0;
        }
    }

    function paiementArticle(){
        if(isset($_SESSION['panier'])){
            $id_user = $_SESSION['userid'];
            $status = "en cours";
            $adresseLivraison = $_POST['adr-livraison'];
            $dte_livraison = date('Y-m-d');
            $prixTotal = montantGlobalTVA();

            try{
                $dbs = new PDO('mysql:host=localhost; dbname=boutiqueenligne', 'root','root');
                $dbs->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);//les noms des champs sont en miniscules
                $dbs->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// les erreurs lanceront des exceptions


            }
            catch (Exception $e){
                die('une erreur est survenue');
            }

            $insert = $dbs->query("INSERT INTO Commande (prixTotal, user, statut, adresseLivraison, dateLivraison) 
                                    VALUES ('$prixTotal', '$id_user', '$status', '$adresseLivraison', '$dte_livraison')");
            if($insert) {
                $commande = $dbs->query("SELECT MAX(idCommande) FROM Commande");
                $idCommande = $commande->fetch();
                $idCommande = $idCommande[0];
                foreach($_SESSION['panier'] as $produit){
                    $idProduit = $produit['idProduit'];
                    $qtite = $produit['qteProduit'];
                    $insertProduitCommande = $dbs->query("INSERT INTO ProduitCommande (idProduit, idCommande, quantite)
                                                                    VALUES ('$idProduit', '$idCommande', '$qtite')");
                }
            }

        }

    }
/**
    function CalculFraisPorts(){
        $error = false;
        $weight_product = "";
        $shipping ="";

        for($i=0; $i < compterArticle(); $i++){
            foreach ( $_SESSION['panier'] as $id=> $prd) {
                $title=$_SESSION['panier'][$id]['libelleProduit'];

            }

        }

        return $shipping;
    }
*/
?>

