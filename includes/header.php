<?php

include 'db_connect.php';

?>
<!DOCTYPE html>
<html>
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../styles/sidebar.css" rel="stylesheet">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="application/javascript" src="../action.js"></script>
</head>

<header>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
        <!-- Brand -->
        <a class="navbar-brand" href="#">Best</a>

        <!-- Links -->
        <ul class="navbar-nav" style="margin-left:auto; margin-right:15px;padding: 3px 47px;">
            <li class="nav-item">
                <a class="nav-link" href="#">Accueil</a>
            </li>
            <li class="nav-item">
                <?php
                if(isset($_SESSION['estAdministrateur']) && $_SESSION['estAdministrateur'] == 1){
                    ?><a class="nav-link" href="../admin/boutique_admin.php">Boutique</a><?php
                }else{
                    ?> <a class="nav-link" href="boutique.php">Boutique</a><?php
                }?>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Inscription</a>
            </li>

            <!-- Dropdown -->
            <li class="nav-item dropdown" >
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Compte
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="panier.php">Panier</a>
                    <a class="dropdown-item" href="#">Favoris</a>
                    <?php
                    session_start();
                    if(isset($_SESSION['estAdministrateur']) && $_SESSION['estAdministrateur'] == 1){
                        ?><a class="dropdown-item" href="../admin/dashboard_admin.php">Admin</a><?php
                    }
                    ?>
                    <div class="dropdown-divider"></div>
                    <?php
                    if(isset($_SESSION['login'])){
                        ?><a class="dropdown-item" style="margin-left:auto;" href="logout.php">Déconnexion</a><?php
                    }
                    else{
                        ?> <a class="dropdown-item" style="margin-left:auto;" href="connexion.php">Connexion</a> <?php
                    }
                    ?>

                </div>
            </li>
        </ul>
    </nav>
    <br>

</header>
<!--
    <body>
        <div class="container-fluid" style="margin-top:80px">
            <h3>Top Fixed Navbar</h3>
            <p>A fixed navigation bar stays visible in a fixed position (top or bottom) independent of the page scroll.</p>
            <h1>Scroll this page to see the effect</h1>
        </div>

</body>
-->
</html>