<?php
include 'includes/header.php';
?>

<?php

    include_once('includes/db_connect.php');

    if(isset($_POST) && !empty($_POST)){
        if (!empty(htmlspecialchars($_POST['username'])) && !empty(htmlspecialchars($_POST['password']))) {
            $req = $dbs->prepare('SELECT * FROM User WHERE userLogin = ?');
            $req->execute(array(strip_tags($_POST['username'])));
            $count = $req->rowCount();
            if($count == 0) $error = 1;
            else{
                $result = $req->fetch();
                if(sha1(strip_tags($_POST['password'])) == $result['userpassword']){
                    $_SESSION['login'] = $result['userlogin'];
                    $_SESSION['userid'] = $result['iduser'];
                    $_SESSION['estAdministrateur'] = $result['isadmin'];

                    if($result['isadmin'] == 0) {
                        header('Location: index.php');
                    }
                    else header('Location: admin/dashboard_admin.php');
                }
                else $error = 2;
            }

        }
        else{
            $error = 'Veuillez remplir tout les gens !';
        }
    }


?>
<html>
    <head>
        <meta charset="utf-8">
        <!-- importer le fichier de style -->
        <link rel="stylesheet" href="styles/.css" media="screen" type="text/css" />
    </head>


    <body>
    <?php
    if(isset($error)){
        if($error == 1) echo '<p>An error occurs. The specified username does not exist.</p>';
        else if($error == 2) echo '<p>An error occurs. The specified username and password does not match.</p>';
    }
    ?>
    <div id="container">
            <!-- zone de connexion -->

            <form action="connexion.php" method="POST">

                <h1 style ="width: 50%;">Connexion</h1>

                <div class="form-group">
                    <label for="login">Nom d'utilisateur</label>
                    <input type="text" class="form-control"  name="username" placeholder="Jean.Paul">
                </div>
                <div class="form-group">
                    <label for="pswd">Mot de passe</label>
                    <input required type="password" class="form-control" name="password" placeholder="*******">
                </div>

                <button type="submit" class="btn btn-secondary" name="submit" style ="text-align: center; margin : auto">Login</button>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="text-align : right">Mot de passe oublié ?</a>
                <?php
                if(isset($erreur) && $erreur == 1) echo ' <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="text-align : right">Votre mot de passe est incorrect. Veuillez le vérifier</a>';
                ?>

            </form>
    </div>
    </body>
</html>

<?php
require_once("includes/footer.php");
?>
