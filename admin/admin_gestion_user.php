<?php

require_once('../includes/header.php');
?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <br/><br/><br/><br/>
    <h1>Bienvenue <?php echo $_SESSION['login']?>,</h1>
    <br/><br/><br/><br/>
    <a href="?action=add">Ajouter un user</a>
    <a href="?action=modifyanddelete">Modifier /Supprimer un user </a><br/><br/>
<?php

    if(isset($_SESSION['login'])) {
        if (isset($_GET['action'])) {

            if ($_GET['action'] == 'add') {
                if(isset($_POST['submit'])) {
                    $usr_email = $_POST['usr_email'];
                    $usr_nom = $_POST['usr_nom'];
                    $usr_prenom = $_POST['usr_prenom'];
                    $usr_dtenaissance = $_POST['usr_dtenaissance'];
                    $usr_login = $_POST['usr_login'];
                    $usr_password = SHA1($_POST['usr_pass']);
                    if ($usr_nom && $usr_prenom && $usr_email && $usr_dtenaissance && $usr_login && $usr_password){
                        $insert = $dbs->prepare("INSERT INTO User VALUES('', ?, ?, ?,? , 0,?, ?)");
                        $insert->execute(Array($usr_email,$usr_nom,$usr_prenom,$usr_dtenaissance,$usr_login,$usr_password));
                    }
                    else {
                        echo 'Veuillez remplir tout les champs';
                    }
                }
                ?>
                <table class="table table-hover">
                <thead>
                <td>Nom</td>
                <td>Prenom</td>
                <td>Email</td>
                <td>Date de Naissance</td>
                <td>Login</td>
                <td>Mot de Passe</td>
                <td>Action</td>
                </thead>
                <thead>
                <form action="admin_gestion_user.php?action=add" method="post">
                    <td><input type="text" name="usr_nom" value="" placeholder="Nom"></td>
                    <td><input type="text" name="usr_prenom" value="" placeholder="Prenom">
                    </td>
                    <td><input type="text" name="usr_email" value="" placeholder="Email"></td>
                    <td><input type="text" name="usr_dtenaissance" value=""
                               placeholder="DatedeNaissance"></td>
                    <td><input type="text" name="usr_login" value="" placeholder="Login"></td>
                    <td><input type="text" name="usr_pass" value=""
                               placeholder="Mot de Passe"></td>

                    <td><input type="submit" value="Ajouter" name="submit"></td>
                    <td></td>
                </form>
                </thead>
                <?php
            } else if ($_GET['action'] == 'modifyanddelete') {
                $select = $dbs->prepare(" SELECT *FROM User");
                $select->execute();
                $result = $select->fetchAll(PDO::FETCH_ASSOC);
                foreach ($result as $user) {
                    echo $user['nom'];
                    ?>
                    <a href="?action=modify&amp;id=<?php echo $user['iduser']; ?>">Modifier</a>
                    <a href="?action=delete&amp;id=<?php echo $user['iduser']; ?>">X</a><br/><br/>
                    <?php
                }

            } else if ($_GET['action'] == 'modify') {
                if(!isset($_POST['modify'])){
                    $select = $dbs->prepare("SELECT * FROM User WHERE idUser = ?");
                    $select->execute([$_GET['id']]);

                    $data = $select->fetchAll(PDO::FETCH_ASSOC);
                    $data = $data[0];

                    ?>
                    <form action="admin_gestion_user.php?action=modify" method="post">
                    <input type="hidden" name="modify" value="Modifier">
                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                    <td><input type="text" name="usr_nom" value="<?php echo $data['nom']; ?>" placeholder="Nom"></td>
                    <td><input type="text" name="usr_prenom" value="<?php echo $data['prenom']; ?>"
                               placeholder="Prenom"></td>
                    <td><input type="text" name="usr_email" value="<?php echo $data['email']; ?>"
                               placeholder="Email"></td>
                    <td><input type="text" name="usr_dtenaissance" value="<?php echo $data['dtenaissance']; ?>"
                               placeholder="DatedeNaissance"></td>
                    <td><input type="text" name="usr_login" value="<?php echo $data['userlogin']; ?>"
                               placeholder="Login"></td>
                    <td><input type="text" name="usr_pass" value="<?php echo $data['userpassword']; ?>"
                               placeholder="Mot de Passe"></td>


                    <td><input type="submit" name="submit" value="Modifier"/></td>
                    <td></td>
                </form>
                    <?php
                }else if (isset($_POST['submit'])) {

                    $usr_email = $_POST['usr_email'];
                    $usr_nom = $_POST['usr_nom'];
                    $usr_prenom = $_POST['usr_prenom'];
                    $usr_dtenaissance = $_POST['usr_dtenaissance'];
                    $usr_login = $_POST['usr_login'];
                    $usr_password = SHA1($_POST['usr_pass']);

                    $update = $dbs->prepare("UPDATE User SET email = ?,nom=?, prenom=? , dteNaissance=? , userLogin=?, userPassword=? WHERE idUser = ?");
                    $update->execute(Array($usr_email, $usr_nom, $usr_prenom, $usr_dtenaissance, $usr_login, $usr_password, $_POST['id']));
                    header('Location: admin_gestion_user.php?action=modifyanddelete');


                }

            } else if ($_GET['action'] == 'delete') {
                $delete = $dbs->prepare(" DELETE  FROM User WHERE iduser = ?");
                $delete->execute([$_GET['id']]);
                header('Location: admin_gestion_user.php?action=modifyanddelete');
            } else {
                die('Une erreur s\'est produite.');
            }

        } else {

        }
    }
    ?>
            </table>




<?php
include '../includes/footer.php';
?>