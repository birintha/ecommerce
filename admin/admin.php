<?php
    require_once('../includes/header.php');
?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <br/><br/><br/><br/>
    <h1>Bienvenue <?php echo $_SESSION['login']?>,</h1>
    <br/>

    <a href="?action=add">Ajouter un produit</a>&nbsp&nbsp&nbsp;
    <a href="?action=modifyanddelete">Modifier /Supprimer un produit</a>&nbsp&nbsp&nbsp;

    <a href="?action=add_category">Ajouter une catégorie</a>&nbsp&nbsp&nbsp;
    <a href="?action=modifyanddelete_category">Modifier /Supprimer une catégorie</a>&nbsp&nbsp&nbsp;

    <a href="?action=options">Options</a><br/><br/>

<?php
    try{
        $dbs = new PDO('mysql:host=localhost; dbname=boutiqueenligne', 'root','root');
        $dbs->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
        $dbs->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    }
    catch (Exception $e){
        echo 'une erreur est survenue';
        die();
    }




if(isset($_SESSION['login'])) {
    if (isset($_GET['action'])) {


        if ($_GET['action'] == 'add') {
            if (isset($_POST['submit'])) {
                $libelle = $_POST['libelle'];
                $prix = $_POST['prix'];
                $description = $_POST['description'];
                $stock = $_POST['stock'];

                /**$img = $_FILES['img']['name'];

                $img_tmp = $_FILES['img']['tmp_name'];

                if (!empty($img_tmp)) {

                    $image = explode('.', $img);
                    $image_ext = end($image);
                    if (in_array(strtolower($image_ext), array('png', 'jpg', 'jpeg')) == false) {
                        echo 'Veuillez rentrer une image ayant pour extension : png, jpg ou jpeg';

                    } else {
                        $image_size = getimagesize($img_tmp);
                        if ($image_size['mime'] == 'image/jpeg') {
                            $image_src = imagecreatefromjpeg($img_tmp);
                        } else if ($image_size['mime'] == 'image/png') {
                            $image_src = imagecreatefromjpeg($img_tmp);
                        } else {
                            $image_src = false;
                            echo 'Veuillez rentrer une image valide';
                        }
                        if ($image_src != false) {
                            $image_width = 200;
                            if ($image_size[0] == $image_width) {
                                $image_finale = $image_src;

                            } else {
                                $new_width[0] = $image_width;

                                $new_height = 200;

                                $image_finale = imagecreatetruecolor($new_width[0], $new_height[1]);

                                imagecopyresampled($image_finale, $image_src, 0, 0, 0, 0, $new_width[0], $new_height[1], $image_size[0], $image_size[1]);


                            }

                            imagejpeg($image_finale, 'imgs/' . $libelle . 'jpg');

                        }
                    }
                } else {

                    echo 'Veuillez rentrer une image !';
                }*/

                if ($libelle && $prix && $description && $stock) {

                    $categorie = $_POST['categorie'];
                    $poids = $_POST['weight'];
                    $image = $_POST['img'];
                    $select =$dbs->query("SELECT prix FROM Poids WHERE name = '$poids'");
                    $result = $select->fetch(PDO::FETCH_OBJ);

                    $shipping=$result->prix;

                    $old_prix = $prix;

                    $calcul_prix_shipping = $old_prix + $shipping ;

                    $select = $dbs->query("SELECT TVA FROM Produit");
                    $s1= $select->fetch(PDO::FETCH_OBJ);
                    $tva = $s1->tva;

                    $final_prix_1 = $calcul_prix_shipping + ($calcul_prix_shipping *($tva/100));

                    $insert = $dbs->prepare(" INSERT INTO Produit VALUES('', '$libelle' , '$categorie' , '$prix' , '$description' , '$stock','$poids','$shipping','$tva','$final_prix_1','$image')");
                    $insert->execute();

                } else {
                    echo 'Veuillez remplir tout les champs';
                }
            }
            ?>

            <form action="" method="POST" enctype="multipart/form-data">

                <h3>Titre du produit</h3><input type="text" name="libelle"/>
                <h3>Catégorie</h3><select name="categorie">
                    <?php

                    $select = $dbs->query("SELECT * FROM Catégorie");

                    $result = $select->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($result as $productcategorie) {

                        ?>
                        <option><?php echo $productcategorie['name']; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <h3>Prix</h3><input type="text" name="prix"/>
                <h3>Description</h3><textarea name="description"></textarea>
                <h3>Stock</h3><input type="text" name="stock"/><br/><br/>
                <h3>URL de l'image</h3><input type="text" name="img" placeholder="./imgs/name.jpg"/><br/><br/>
                <h3>Poids : plus de </h3><select name="weight">
                    <?php

                    $select = $dbs->query("SELECT * FROM Poids");

                    $result = $select->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($result as $productpoids) {

                        ?>
                        <option><?php echo $productpoids['name']; ?></option>
                        <?php
                    }
                    ?>


                </select><br/><br/>
                <input type="submit" name="submit"/>

            </form>

            <?php
        } else if ($_GET['action'] == 'modifyanddelete') {
            $select = $dbs->prepare(" SELECT *FROM Produit");
            $select->execute();
            $result = $select->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $product) {
                echo $product['libelle'];
                ?>
                <a href="?action=modify&amp;id=<?php echo $product['idproduit']; ?>">Modifier</a>
                <a href="?action=delete&amp;id=<?php echo $product['idproduit']; ?>">X</a><br/><br/>
                <?php
            }

        } else if ($_GET['action'] == 'modify') {

            $select = $dbs->prepare("SELECT * FROM Produit WHERE idProduit = ?");
            $select->execute([$_GET['id']]);

            $data = $select->fetchAll(PDO::FETCH_ASSOC);
            $data = $data[0];

            ?>

            <form action="" method="POST">

                <h3>Titre du produit</h3><input value="<?php echo $data['libelle'] ?>" type="text" name="libelle"/>
                <h3>Categorie</h3><input value="<?php echo $data['catégorie'] ?>" type="text" name="categorie"/>
                <h3>Prix</h3><input value="<?php echo $data['prix'] ?>" type="text" name="prix"/>
                <h3>Description</h3><textarea name="description"><?php echo $data['description'] ?></textarea>
                <h3>Stock</h3><input value="<?php echo $data['stock'] ?>" type="text" name="stock"/><br/><br/>
                <input type="submit" name="submit" value="Modifier"/>

            </form>

            <?php

            if (isset($_POST['submit'])) {

                $libelle = $_POST['libelle'];
                $categorie = $_POST['categorie'];
                $prix = $_POST['prix'];
                $description = $_POST['description'];
                $stock = $_POST['stock'];

                $update = $dbs->prepare("UPDATE Produit SET libelle = ?,catégorie=?, prix=? , description=? , stock=? WHERE idProduit = ?");
                $update->execute(Array($libelle, $categorie, $prix, $description, $stock, $_GET['id']));

                header('Location: admin.php?action=modifyanddelete');


            }

        } else if ($_GET['action'] == 'delete') {
            $delete = $dbs->prepare(" DELETE  FROM Produit WHERE idProduit = ?");
            $delete->execute([$_GET['id']]);
            header('Location: admin.php?action=modifyanddelete');
        } else if ($_GET['action'] == 'add_category') {
            if (isset($_POST['submit'])) {
                $name = $_POST['name'];

                if ($name) {
                    $insert = $dbs->prepare(" INSERT INTO Catégorie VALUES('', '$name')");
                    $insert->execute();

                } else {
                    echo 'Veuillez remplir tout les champs';
                }
            }

            ?>

            <form action="" method="post">
                <h3>Titre de la catégorie: </h3><input type="text" name="name"/> <br/><br/>
                <input type="submit" name="submit" value="Ajouter Catégorie"/>
            </form>
            <?php

        } else if ($_GET['action'] == 'modifyanddelete_category') {

            $select = $dbs->prepare(" SELECT *FROM Catégorie");
            $select->execute();
            $result = $select->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $product_category) {
                echo $product_category['name'];
                ?>
                <a href="?action=modify_category&amp;id=<?php echo $product_category['idcatégorie']; ?>">Modifier</a>
                <a href="?action=delete_category&amp;id=<?php echo $product_category['idcatégorie']; ?>">X</a><br/><br/>
                <?php
            }


        } else if ($_GET['action'] == 'modify_category') {
            $select = $dbs->prepare("SELECT * FROM Catégorie WHERE idcatégorie = ?");
            $select->execute([$_GET['id']]);

            $data = $select->fetchAll(PDO::FETCH_ASSOC);
            $data = $data[0];

            ?>

            <form action="" method="POST">

                <h3>Titre du Catégorie</h3><input value="<?php echo $data['name'] ?>" type="text"
                                                  name="titre_catégorie"/>
                <input type="submit" name="submit" value="Modifier"/>

            </form>

            <?php

            if (isset($_POST['submit'])) {

                $titre_catégorie = $_POST['titre_catégorie'];

                $update = $dbs->prepare("UPDATE Catégorie SET name = ? WHERE idcatégorie = ?");
                $update->execute(Array($titre_catégorie, $_GET['id']));

                header('Location: admin.php?action=modifyanddelete_category');


            }


        } else if ($_GET['action'] == 'delete_category') {

            $delete = $dbs->prepare(" DELETE  FROM Catégorie WHERE idcatégorie = ?");
            $delete->execute([$_GET['id']]);
            header('Location: admin.php?action=modifyanddelete_category');

        } else if ($_GET['action'] == 'options') {

            ?>
            <h2>Frais de port:</h2><br/>
            <h3>Options de Poids (plus de) </h3>

            <?php

            $select = $dbs->query("SELECT * FROM Poids");
            $result = $select->fetchAll(PDO::FETCH_ASSOC);

            foreach ($result as $product) {
                ?>
                <form action="" method="post">
                    <input type="text" name="weight" value="<?php echo $product['name']; ?>"/><a
                            href="?action=modify_weight&amp;name=<?php echo $product['name'] ?>">Modifier</a>

                </form>
                <?php
            }
            $select =$dbs->query("SELECT tva FROM Produit");
            $result = $select->fetch(PDO::FETCH_OBJ);

            if(isset($_POST['submit2'])){
                $tva=$_POST['tva'];
                if($tva){

                    $update=$dbs->query("UPDATE Produit SET TVA = $tva");
                }
            }

            ?>
            <h3>TVA:</h3>
            <form action="" method ="post">
                <input type="text" name="tva"value ="<?php echo $result->tva;?>"/>
                <input type="submit" name="submit2" value ="Modifier">
            </form>
            <?php



        }else if ($_GET['action'] == 'modify_weight') {

            $old_weight = $_GET['name'];

            $select = $dbs->query("SELECT * FROM Poids WHERE name = $old_weight");
            $result = $select->fetchAll(PDO::FETCH_ASSOC);

            if (isset($_POST['submit'])) {
                $weight = $_POST['weight'];
                $price = $_POST['price'];

                if ($weight && $price) {
                    $update = $dbs->query("UPDATE Poids SET name = '$weight',prix ='$price' WHERE name = $old_weight");


                }
            }

            foreach ($result as $product_poids) {
                ?>
                    <h2>Frais de port:</h2>
                    <h3>Options de Poids </h3>

                    <form actions="" method="post">

                        <br/><h3>Poids (plus de) :</h3><input type="text" name="weight" value="<?php echo $_GET['name']; ?>"/>
                        <h3>Correspond a</h3><input type="text" name="price" value="<?php echo $product_poids['prix']; ?>"/>
                        Euros<br/>
                        <input type="submit" name="submit" value="Modifier"/>
                    </form>

                <?php
            }
        } else {
            die('Une erreur s\'est produite.');
        }
    }
}

?>
