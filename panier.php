<br/><br/><br/>
<?php
    require_once ('includes/header.php');
    require_once('includes/function_panier.php');

    $erreur =false;
    //if ?alors if ?alors
    $action = (isset($_POST['action'])?$_POST['action']:(isset($_GET['action'])?$_GET['action']:null));



    if($action!=null){
        if(!in_array($action,array('ajout','suppression','modifier','paiement')))
            $erreur = true;
            $id = (isset($_POST['id'])?$_POST['id']: (isset($_GET['id']) ? $_GET['id']:null));
            $l  = (isset($_POST['l'])?$_POST['l']  : (isset($_GET['l'])  ? $_GET['l']:null));
            $q  = (isset($_POST['q'])?$_POST['q']  : (isset($_GET['q'])  ? $_GET['q']:null));
            $p  = (isset($_POST['p'])?$_POST['p']  : (isset($_GET['p'])  ? $_GET['p']:null));

            $idProduit = (isset($_POST['idProduit'])?$_POST['idProduit']: (isset($_GET['idProduit']) ? $_GET['idProduit']:null));

            $l = preg_replace('#\v#', '', $l);
            $p = floatval($p);

            if(is_array($q)){
                $QteProduit = array();
                $i=0;
                foreach ($q as $contenu){
                    $QteProduit[$i++] = intval($contenu);

                }
            }else {

                $q = intval($q);

            }


    }


    if(!$erreur){
        switch ($action){
            case "ajout" :
                ajouterArticle($id,$l,$q,$p);
                break;

            case "suppression" :
                supprimerArticle($idProduit);
                break;

            case "modifier" :
                    modifierQteArticle($idProduit,$q);
                break;

            case "paiement" :
                paiementArticle();
                break;

            Default:
                break;

        }
    }
?>
    <br/><br/>

    <form method="post" action="">
        <table width = "400" class="table">
           <tr>
               <td colspan ="4">Votre Panier</td>
           </tr>

            <tr>
                <td>Libellé Produit</td>
                <td>Prix unitaire</td>
                <td>Quantité</td>
                <td>TVA</td>
                <td>Supprimer</td>
                <td>Modifier</td>
            </tr>

            <?php

                if(isset($_GET['deletepanier']) && $_GET['deletepanier'] == true){

                    supprimerPanier();
                }

                if(creationPanier()) {


                    $nbProduits = (isset($_SESSION['panier'])) ? count($_SESSION['panier']) : 0;

                    if ($nbProduits <= 0) {
                        echo '<p style="font-size : 20px ; color:red">Oops, votre panier est vide !</p>';

                    } else {
                            foreach ($_SESSION['panier'] as $id => $prd){

                                ?>
                                <tr>

                                    <td><br/><?php echo $_SESSION['panier'][$id]['libelleProduit']; ?></td>
                                    <td><br/><?php echo $_SESSION['panier'][$id]['prixProduit']; ?></td>
                                    <td><br/><input name="q" value="<?php echo $_SESSION['panier'][$id]['qteProduit']; ?>"
                                                    size="5"/></td>
                                    <td><br/><?php echo $_SESSION['tva'] . "%"; ?></td>
                                    <td>
                                        <form>
                                            <input type="hidden" name="action" value="suppression"/>
                                            <input type="hidden" name="idProduit" value="<?php echo $_SESSION['panier'][$id]['idProduit']?>"/>
                                            <input type="submit" value="Supprimer"/>
                                        </form>
                                    </td>
                                    <td>
                                        <input type="hidden" name="action" value="modifier"/>
                                        <input type="hidden" name="idProduit" value="<?php echo $_SESSION['panier'][$id]['idProduit']?>"/>
                                        <input type="submit" value="Modifier"/>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="2"><br/>
                                    <p>Total : <?php echo MontantGlobal(); ?></p><br/>
                                    <p>Total avec TVA : <?php echo MontantGlobalTVA(); ?></p>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                    <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
                                    <script>paypal.Buttons().render('body');</script>
                                </td>

                                <td colspan="4">
                                    <a href="?deletepanier=true">Supprimer le panier</a>
                                </td>
                                <td colspan="4">
                                    <form method="post">
                                        <input type="text" id="adr-livraison" name="adr-livraison" placeholder="Adresse de livraison" required/>
                                        <input type ="submit" value="payer"/>
                                        <input type="hidden" name="action" value="paiement"/>
                                    </form>

                                </td>
                            </tr>
                        <!--
                            <tr class="confirm-paiement" hidden="hidden">
                                <td colspan="4">
                                    <input type="text" placeholder="Adresse de livraison" required>
                                    <input type="submit" value="Confirmer le paiement">

                                </td>
                            </tr>-->

                        <?php
                        }
                    }
                    ?>
        </table>
    </form>

<?php

    require_once('includes/footer.php');
?>
